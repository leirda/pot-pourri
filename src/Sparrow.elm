import Browser
import Element as El
import Element.Input as Input
import Html exposing (Html)
import Svg exposing (..)
import Svg.Attributes exposing (..)


-- MAIN


main =
  Browser.sandbox
    { init = init
    , update = update
    , view = view
    }



-- MODEL   


type alias Model =
  ( Bool, Bool )


init : Model
init =
  ( False, False )



-- UPDATE


type Msg
  = ToggleUpFold Bool
  | ToggleDownFold Bool


update : Msg -> Model -> Model
update msg ( oldUpFoldState, oldDownFoldState ) =
  case msg of
    ToggleDownFold newDownFoldState ->
      ( oldUpFoldState, newDownFoldState )

    ToggleUpFold newUpFoldState ->
      ( newUpFoldState, oldDownFoldState )




-- VIEW


view : Model -> Html Msg
view model =
  let
    checkbox : (Bool -> Msg) -> (( Bool, Bool ) -> Bool) -> El.Element Msg
    checkbox toggleFold getter =
      Input.checkbox
        [ El.height El.fill ]
        { onChange = toggleFold
        , icon = fold >> El.html >> El.el [ El.centerY ]
        , checked = getter model
        , label = Input.labelHidden "fold"
        }
  in
  El.column
    [ El.height El.fill ]
    [ checkbox ToggleUpFold Tuple.first
    , checkbox ToggleDownFold Tuple.second
    ]
    |> El.layout [ El.height El.fill ]


fold : Bool -> Html msg
fold bool =
  let
    foldedSparrow : String
    foldedSparrow = "0.1,0.1 0.9,0.5 0.1,0.9"

    unfoldedSparrow : String
    unfoldedSparrow = "0.1,0.1 0.5,0.9 0.9,0.1"
  in
  svg
    [ width "24"
    , height "24"
    , viewBox "0 0 1 1"
    ]
    [ if bool then
        arrow foldedSparrow
      else
        arrow unfoldedSparrow
    ]


arrow : String -> Svg msg
arrow coords =
  polyline  
    [ fill "none"
    , stroke "black"
    , strokeWidth "0.15"
    , strokeLinejoin "round"
    , strokeLinecap "round"
    , points coords
    ]
    []
